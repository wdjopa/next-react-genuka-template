import React, { useEffect } from "react";
import { Box, Button, Center, Container, Flex, Heading, Image, SimpleGrid, Spinner, Stack, StackDivider, Text, useColorModeValue } from "@chakra-ui/react";
import { getProduct, getProductById, useGenukaDispatch, useGenukaState } from "../../store/genukaStore";

function FeaturedProduct({ company, product_id }) {
  const { products } = useGenukaState();
  const dispatch = useGenukaDispatch();
  const price_color = useColorModeValue("gray.900", "gray.400");
  const descriptionBorderColor = useColorModeValue("gray.200", "gray.600");
  const buyButtonBgColor = useColorModeValue("gray.900", "gray.50");
  const buyButtonTextColor = useColorModeValue("white", "gray.900");
  useEffect(() => {
    if (company && !products[product_id]) {
      getProductById(dispatch, company.id, product_id);
    }
  }, [product_id]);

  if (!products[product_id])
    return (
      <Center>
        <Spinner />
      </Center>
    );
  let product = products[product_id];
  return (
    <Container maxW={"7xl"}>
      <SimpleGrid columns={{ base: 1, lg: 2 }} spacing={{ base: 8, md: 10 }} py={{ base: 18, md: 24 }}>
        <Flex>
          <Image rounded={"md"} alt={"image " + product.name} src={product.medias[0].link} fit={"cover"} align={"center"} w={"100%"} h={{ base: "100%", sm: "400px", lg: "500px", objectFit: "contain" }} />
        </Flex>
        <Stack spacing={{ base: 6, md: 10 }}>
          <Box as={"header"}>
            <Heading lineHeight={1.1} fontWeight={600} fontSize={{ base: "2xl", sm: "4xl", lg: "5xl" }}>
              {product.name}
            </Heading>
            <Text color={price_color} fontWeight={300} fontSize={"4xl"}>
              {product.discounted_price}
              {company.currency?.symbol}
            </Text>
          </Box>

          <Stack spacing={{ base: 4, sm: 6 }} direction={"column"} divider={<StackDivider borderColor={descriptionBorderColor} />}>
            <div fontSize={"lg"} dangerouslySetInnerHTML={{ __html: product.description }} />
          </Stack>

          <Button
            rounded={"none"}
            w={"full"}
            mt={8}
            size={"lg"}
            py={"7"}
            bg={buyButtonBgColor}
            color={buyButtonTextColor}
            textTransform={"uppercase"}
            _hover={{
              transform: "translateY(2px)",
              boxShadow: "lg",
            }}
          >
           Ajouter au panier
          </Button>
          <Button
            rounded={"none"}
            w={"full"}
            mt={8}
            size={"lg"}
            py={"7"}
            bg={buyButtonBgColor}
            color={buyButtonTextColor}
            textTransform={"uppercase"}
            _hover={{
              transform: "translateY(2px)",
              boxShadow: "lg",
            }}
          >
            Acheter maintenant
          </Button>
        </Stack>
      </SimpleGrid>
    </Container>
  );
}

export default FeaturedProduct;
