import { ChevronRightIcon } from "@chakra-ui/icons";
import { Box, Button, Center, Flex, Grid, GridItem, Heading, Spacer, Spinner } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { useGenukaState, getCollection, useGenukaDispatch } from "../../store/genukaStore";
import ProductCard from "./ProductCard";
import NextLink from "next/link";
function FeaturedCollection({ collection_id, company }) {
  const { collections } = useGenukaState();
  const dispatch = useGenukaDispatch();
  useEffect(() => {
    if (company && !collections[collection_id]) {
      getCollection(dispatch, company.id, collection_id);
    }
  }, [collection_id]);

  if (!collections[collection_id]) return <Center><Spinner/></Center>;
  let collection = collections[collection_id].collection;
  let products = collections[collection_id].products.data;
  return (
    <Box my="2rem">
      <Flex alignItems={"center"}>
        <Box>
          <Heading as="h3" size="lg">
            {collection.name}
          </Heading>
          <div>{collection.description}</div>
        </Box>
        <Spacer />
        <Box>
          <NextLink href={"/collection/" + collection.id} passHref>
            <Button colorScheme="orange" variant="link">
              See more <ChevronRightIcon />
            </Button>
          </NextLink>
        </Box>
      </Flex>
      <Flex flexWrap={"wrap"} justifyContent={"space-between"} alignItems={"center"}>
        {products.map((product) => {
          return <ProductCard key={product.id} product={product} currency={company.currency} default_img={company.logo} />;
        })}
      </Flex>
    </Box>
  );
}

export default FeaturedCollection;
