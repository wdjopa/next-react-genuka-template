import { Box, Flex, Heading } from "@chakra-ui/react";
import React from "react";
import Pagination from "../Pagination";
import ProductCard from "./ProductCard";

function CollectionDetailPage({ company, collection, products, pagination }) {
  return !collection ? (
    <>Loading</>
  ) : (
    <Box my="2rem">
      <Flex alignItems={"center"}>
        <Box>
          <Heading as="h3" size="lg">
            {collection.name}
          </Heading>
          <div>{collection.description}</div>
        </Box>
      </Flex>
      <Flex flexWrap={"wrap"} justifyContent={"space-between"} alignItems={"center"}>
        {products.map((product) => {
          return <ProductCard key={product.id} product={product} currency={company.currency} default_img={company.logo} />;
        })}
      </Flex>
      <Pagination pagination={pagination} />
    </Box>
  );
}

export default CollectionDetailPage;
