import { Box, Flex, Heading } from "@chakra-ui/react";
import React from "react";
import Pagination from "../Pagination";
import CollectionCard from "./CollectionCard";

function CollectionList({collections_list, pagination, company}) {

  return (
    <Box my="10">
      <Heading >Collections</Heading>
      <Flex flexWrap={"wrap"} justifyContent={"space-between"} alignItems={"center"}>
        {collections_list &&
          collections_list.map((collection) => {
            return <CollectionCard key={collection.id} collection={collection} default_img={company.logo} />;
          })}
      </Flex>
      <Pagination pagination={pagination}/>
    </Box>
  );
}

export default CollectionList;
