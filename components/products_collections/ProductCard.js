import { Flex, Circle, Box, Image, Badge, useColorModeValue, Icon, chakra, Tooltip } from "@chakra-ui/react";
import { BsStar, BsStarFill, BsStarHalf } from "react-icons/bs";
import { FiShoppingCart } from "react-icons/fi";
import NextLink from "next/link";

function Rating({ rating, numReviews }) {
  return (
    <Flex alignItems="flex-start" flexDirection={"column"}>
      <Flex>
        {Array(5)
          .fill("")
          .map((_, i) => {
            const roundedRating = Math.round(rating * 2) / 2;
            if (roundedRating - i >= 1) {
              return <BsStarFill size="xs" key={i} style={{ marginLeft: "1" }} color={i < rating ? "orange.500" : "gray.300"} />;
            }
            if (roundedRating - i === 0.5) {
              return <BsStarHalf size="xs" key={i} style={{ marginLeft: "1" }} />;
            }
            return <BsStar key={i} style={{ marginLeft: "1" }} />;
          })}
      </Flex>
      <Box as="span" ml="2" color="gray.600" fontSize="xs">
        {numReviews} review{numReviews > 1 && "s"}
      </Box>
    </Flex>
  );
}

function ProductCard({ product, currency, default_img }) {
  product.isNew = Date.now() - new Date(product.created_at).getTime() < 1000 * 60 * 60 * 24 * 7; // a product created one week ago is new
  return (
    <NextLink href={"/product/" + product.slug} passHref>
      <Flex py={"5"} w="2xs" alignItems="center" justifyContent="center">
        <Box bg={useColorModeValue("white", "gray.800")} maxW="2xs" borderWidth="1px" rounded="lg" shadow="xs">
          {product.isNew && <Circle size="10px" position="absolute" top={2} right={2} bg="red.200" />}

          <Box height="2xs" width="2xs">
            <Image src={product.medias[0]?.thumb || default_img} alt={`Picture of ${product.name}`} roundedTop="lg" style={{ height: "100%", width: "100%", margin: "auto", objectFit: "cover" }} />
          </Box>

          <Box p="6">
            <Box d="flex" alignItems="baseline">
              {product.isNew && (
                <Badge rounded="full" px="2" fontSize="0.8em" colorScheme="red">
                  New
                </Badge>
              )}
            </Box>
            <Flex mt="1" justifyContent="space-between" alignContent="center">
              <Box fontSize="xl" fontWeight="semibold" as="h4" lineHeight="tight" isTruncated>
                {product.name}
              </Box>
              <Tooltip label="Add to cart" bg="white" placement={"top"} color={"gray.800"} fontSize={"1.2em"}>
                <chakra.a href={"#"} display={"flex"}>
                  <Icon as={FiShoppingCart} h={7} w={7} size="xs" alignSelf={"center"} />
                </chakra.a>
              </Tooltip>
            </Flex>

            <Flex justifyContent="space-between" alignContent="center">
              <Rating rating={product.avg_reviews || 0} numReviews={product.total_reviews} />
              <>
                <Box fontSize="xl" color={useColorModeValue("orange.500", "white")}>
                  {product.discounted_price.toFixed(currency?.datas?.decimals || 0)}{" "}
                  <Box as="span" color={"gray.600"} fontSize="lg">
                    {currency?.symbol || "FCFA"}
                  </Box>
                </Box>
                {product.comparaison_price > product.price && (
                  <Box fontSize="md" textDecoration={"line-through"} color={"gray.800"}>
                    {product.comparaison_price?.toFixed(currency?.datas?.decimals || 0)}{" "}
                    <Box as="span" color={"gray.600"} fontSize="lg">
                      {currency?.symbol || "FCFA"}
                    </Box>
                  </Box>
                )}
              </>
            </Flex>
          </Box>
        </Box>
      </Flex>
    </NextLink>
  );
}

export default ProductCard;
