import { Box, Flex, Heading } from "@chakra-ui/react";
import React from "react";
import Pagination from "../Pagination";
import ProductCard from "./ProductCard";

function ProductList({products_list, pagination, company}) {

  return (
    <Box my="10">
      <Heading >Produits</Heading>
      <Flex flexWrap={"wrap"} justifyContent={"space-between"} alignItems={"center"}>
        {products_list &&
          products_list.map((product) => {
            return <ProductCard key={product.id} product={product} default_img={company.logo} />;
          })}
      </Flex>
      <Pagination pagination={pagination}/>
    </Box>
  );
}

export default ProductList;
