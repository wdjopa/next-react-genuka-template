import { Badge, Box, Circle, Flex, Image, useColorModeValue } from "@chakra-ui/react";
import NextLink from "next/link";

function CollectionCard({ collection, default_img }) {
  collection.isNew = Date.now() - new Date(collection.created_at).getTime() < 1000 * 60 * 60 * 24 * 7; // a collection created one week ago is new
  return (
    <NextLink href={"/collection/" + collection.id} passHref>
      <Flex py={"5"} w="sm" alignItems="center" justifyContent="center">
        <Box bg={useColorModeValue("white", "gray.800")} maxW="sm" borderWidth="1px" rounded="lg" shadow="lg">
          {collection.isNew && <Circle size="10px" position="absolute" top={2} right={2} bg="red.200" />}

          <Box height="sm" width="sm">
            <Image src={collection.medias[0]?.link || default_img} alt={`Picture of ${collection.name}`} roundedTop="lg" style={{ height: "100%", width: "100%", margin: "auto", objectFit: "cover" }} />
          </Box>

          <Box p="6">
            <Box d="flex" alignItems="baseline">
              {collection.isNew && (
                <Badge rounded="full" px="2" fontSize="0.8em" colorScheme="red">
                  New
                </Badge>
              )}
            </Box>
            <Flex mt="1" justifyContent="space-between" alignContent="center">
              <Box fontSize="xl" fontWeight="semibold" as="h4" lineHeight="tight" isTruncated>
                {collection.name}{" "}
              </Box>
              <Box>
                <Badge rounded="full" px="2" fontSize="sm" colorScheme="orange">
                  {collection.total_products} products
                </Badge>
              </Box>
            </Flex>
          </Box>
        </Box>
      </Flex>
    </NextLink>
  );
}

export default CollectionCard;
