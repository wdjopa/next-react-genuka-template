import { Container } from "@chakra-ui/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";

export default function Layout({ company, children, meta_tags , title = ""}) {
  if(!company)
  return <></>
  return (
    <div>
      <Header company={company} meta_tags={meta_tags} title={title}/>
      <Container maxW={"container.xl"}>{children}</Container>
      <Footer company={company} />
    </div>
  );
}
