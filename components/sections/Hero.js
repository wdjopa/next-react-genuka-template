import React from "react";
import NextLink from "next/link";
import { Button, Flex, Heading, Image, Stack, Text, useBreakpointValue } from "@chakra-ui/react";

export default function Hero({ section, color = "orange.400" }) {
  console.log(section);
  const { title, subtitle, paragraph, image, image_left: reverse, action_buttons } = section;
  const btn_1 = action_buttons ? action_buttons[0] : undefined;
  const btn_2 = action_buttons ? action_buttons[1] : undefined;
  const height = useBreakpointValue({ base: "20%", md: "30%" });
  return reverse ? (
    <Stack my={12} direction={{ base: "column", md: "row" }}>
      <Flex flex={1} align={"center"} justify={"flex-start"}>
        <Stack spacing={6} w={"full"} maxW={"lg"}>
          <Heading fontSize={{ base: "3xl", md: "4xl", lg: "5xl" }}>
            {title && (
              <>
                <Text
                  as={"span"}
                  position={"relative"}
                  _after={{
                    content: "''",
                    width: "full",
                    height,
                    position: "absolute",
                    bottom: 1,
                    left: 0,
                    bg: color,
                    zIndex: -1,
                  }}
                >
                  {title}
                </Text>
                <br />
              </>
            )}
            {subtitle && (
              <Text color={color} as={"span"}>
                {subtitle}
              </Text>
            )}{" "}
          </Heading>
          <Text fontSize={{ base: "md", lg: "lg" }} color={"gray.500"}>
            {paragraph}
          </Text>
          <Stack direction={{ base: "column", md: "row" }} spacing={4}>
            {btn_1?.text && btn_1?.link && (
              <NextLink to={btn_1.link}>
                <Button
                  rounded={"full"}
                  bg={color}
                  color={"white"}
                  _hover={{
                    bg: color + ".500",
                  }}
                >
                  {btn_1.text}
                </Button>
              </NextLink>
            )}
            {btn_2?.text && btn_2?.link && (
              <NextLink to={btn_2.link}>
                <Button rounded={"full"}> {btn_2.text}</Button>
              </NextLink>
            )}
          </Stack>
        </Stack>
      </Flex>
      <Flex flex={1} justify={"flex-end"}>
        <Image alt={"Login Image"} objectFit={"cover"} src={image} />
      </Flex>
    </Stack>
  ) : (
    <Stack my={12} direction={{ base: "column", md: "row" }}>
      <Flex flex={1} justify={"flex-start"}>
        <Image alt={"Login Image"} objectFit={"cover"} src={image} />
      </Flex>
      <Flex flex={1} align={"center"} justify={"flex-end"}>
        <Stack spacing={6} w={"full"} maxW={"lg"}>
          <Heading fontSize={{ base: "3xl", md: "4xl", lg: "5xl" }}>
            {title && (
              <>
                <Text
                  as={"span"}
                  position={"relative"}
                  _after={{
                    content: "''",
                    width: "full",
                    height,
                    position: "absolute",
                    bottom: 1,
                    left: 0,
                    bg: color,
                    zIndex: -1,
                  }}
                >
                  {title}
                </Text>
                <br />
              </>
            )}
            {subtitle && (
              <Text color={color} as={"span"}>
                {subtitle}
              </Text>
            )}{" "}
          </Heading>
          <Text fontSize={{ base: "md", lg: "lg" }} color={"gray.500"}>
            {paragraph}
          </Text>
          <Stack direction={{ base: "column", md: "row" }} spacing={4}>
            {btn_1?.text && btn_1?.link && (
              <NextLink to={btn_1.link}>
                <Button
                  rounded={"full"}
                  bg={color}
                  color={"white"}
                  _hover={{
                    bg: color + ".500",
                  }}
                >
                  {btn_1.text}
                </Button>
              </NextLink>
            )}
            {btn_2?.text && btn_2?.link && (
              <NextLink to={btn_2.link}>
                <Button rounded={"full"}> {btn_2.text}</Button>
              </NextLink>
            )}
          </Stack>
        </Stack>
      </Flex>
    </Stack>
  );
}
