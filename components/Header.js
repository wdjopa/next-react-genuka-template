import React from "react";
import Head from "next/head";
import { Box, Flex, Text, IconButton, Button, Stack, Collapse, Icon, Link, Popover, PopoverTrigger, PopoverContent, useColorModeValue, useBreakpointValue, useDisclosure, Image, Avatar } from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon, ChevronDownIcon, ChevronRightIcon } from "@chakra-ui/icons";
import { Container } from "@chakra-ui/react";
import NextLink from "next/link";
import { TEMPLATE_NAME } from "../utils/configs";
function WithSubnavigation({ company }) {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box py={{ base: 2 }} px={{ base: 4 }} borderBottom={1} borderStyle={"solid"} borderColor={useColorModeValue("gray.200", "gray.900")}>
      <Container maxW={"container.xl"}>
        <Flex bg={useColorModeValue("white", "gray.800")} color={useColorModeValue("gray.600", "white")} minH={"60px"} align={"center"}>
          <Flex flex={{ base: 1, md: "auto" }} ml={{ base: -2 }} display={{ base: "flex", md: "none" }}>
            <IconButton onClick={onToggle} icon={isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />} variant={"ghost"} aria-label={"Toggle Navigation"} />
          </Flex>
          <Flex flex={{ base: 1 }} justify={{ base: "center", md: "start" }}>
            <NextLink href="/">
              <Text textAlign={useBreakpointValue({ base: "center", md: "left" })} fontFamily={"heading"} color={useColorModeValue("gray.800", "white")}>
                {company.template && company.template.theme && company.template.theme.rounded_logo ? <Avatar src={company.logo} alt={company.name + "logo"} /> : <Image src={company.logo} alt={company.name + "logo"} style={{ height: "50px", objectFit: "contain" }} />}
              </Text>
            </NextLink>

            <Flex display={{ base: "none", md: "flex" }} ml={10}>
              <DesktopNav />
            </Flex>
          </Flex>

          <Stack flex={{ base: 1, md: 0 }} justify={"flex-end"} direction={"row"} spacing={6}>
            <Button as={"a"} fontSize={"sm"} fontWeight={400} variant={"link"} href={"/register"}>
              Inscription
            </Button>
            <Button
              display={{ base: "none", md: "inline-flex" }}
              fontSize={"sm"}
              fontWeight={600}
              color={"white"}
              bg={"orange.400"}
              href={"/login"}
              _hover={{
                bg: "orange.300",
              }}
            >
              Connexion
            </Button>
          </Stack>
        </Flex>

        <Collapse in={isOpen} animateOpacity>
          <MobileNav />
        </Collapse>
      </Container>
    </Box>
  );
}

const DesktopNav = () => {
  const linkColor = useColorModeValue("gray.600", "gray.200");
  const linkHoverColor = useColorModeValue("gray.800", "white");
  const popoverContentBgColor = useColorModeValue("white", "gray.800");

  return (
    <Stack direction={"row"} align={"center"} spacing={4}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={"hover"} placement={"bottom-start"}>
            <PopoverTrigger>
              <Link
                p={2}
                href={navItem.href ?? "#"}
                fontSize={"sm"}
                fontWeight={500}
                color={linkColor}
                _hover={{
                  textDecoration: "none",
                  color: linkHoverColor,
                }}
              >
                {navItem.label}
              </Link>
            </PopoverTrigger>

            {navItem.children && (
              <PopoverContent border={0} boxShadow={"xl"} bg={popoverContentBgColor} p={4} rounded={"xl"} minW={"sm"}>
                <Stack>
                  {navItem.children.map((child) => (
                    <DesktopSubNav key={child.label} {...child} />
                  ))}
                </Stack>
              </PopoverContent>
            )}
          </Popover>
        </Box>
      ))}
    </Stack>
  );
};

const DesktopSubNav = ({ label, href, subLabel }) => {
  return (
    <Link href={href} role={"group"} display={"block"} p={2} rounded={"md"} _hover={{ bg: useColorModeValue("orange.50", "gray.900") }}>
      <Stack direction={"row"} align={"center"}>
        <Box>
          <Text transition={"all .3s ease"} _groupHover={{ color: "orange.400" }} fontWeight={500}>
            {label}
          </Text>
          <Text fontSize={"sm"}>{subLabel}</Text>
        </Box>
        <Flex transition={"all .3s ease"} transform={"translateX(-10px)"} opacity={0} _groupHover={{ opacity: "100%", transform: "translateX(0)" }} justify={"flex-end"} align={"center"} flex={1}>
          <Icon color={"orange.400"} w={5} h={5} as={ChevronRightIcon} />
        </Flex>
      </Stack>
    </Link>
  );
};

const MobileNav = () => {
  return (
    <Stack bg={useColorModeValue("white", "gray.800")} p={4} display={{ md: "none" }}>
      {NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  );
};

const MobileNavItem = ({ label, children, href }) => {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Flex
        py={2}
        as={Link}
        href={href ?? "#"}
        justify={"space-between"}
        align={"center"}
        _hover={{
          textDecoration: "none",
        }}
      >
        <Text fontWeight={600} color={useColorModeValue("gray.600", "gray.200")}>
          {label}
        </Text>
        {children && <Icon as={ChevronDownIcon} transition={"all .25s ease-in-out"} transform={isOpen ? "rotate(180deg)" : ""} w={6} h={6} />}
      </Flex>

      <Collapse in={isOpen} animateOpacity style={{ marginTop: "0!important" }}>
        <Stack mt={2} pl={4} borderLeft={1} borderStyle={"solid"} borderColor={useColorModeValue("gray.200", "gray.700")} align={"start"}>
          {children &&
            children.map((child) => (
              <Link key={child.label} py={2} href={child.href}>
                {child.label}
              </Link>
            ))}
        </Stack>
      </Collapse>
    </Stack>
  );
};

const NAV_ITEMS = [
  {
    label: "Home",
    href: "/",
  },
  {
    label: "Products",
    children: [
      {
        label: "Collections",
        subLabel: "See all collection",
        href: "/collections",
      },
      {
        label: "Products",
        subLabel: "See all products",
        href: "/products",
      },
    ],
  },

  {
    label: "Blogs",
    href: "/blogs",
  },
  {
    label: "Contact",
    href: "/contact",
  },
];

function Header({ company, meta_tags, title }) {
  return (
    <div>
      {meta_tags ? (
        meta_tags
      ) : (
        <Head key="main">
          <title>
            {title} - {company.name} | {company.description || "Boutique créé avec Genuka"}
          </title>
          <meta name="description" content={company.description || "Commandez vos produits chez " + company.name} />
          <meta name="keywords" content={company?.description?.split(" ").join(", ")} />
          <meta name="author" content={"Genuka Platform"} />
          <link rel="icon" type="image/png" href={company.logo} />
          <meta name="robots" content="index, follow" />

          <meta property="og:title" content={company.name} />
          <meta property="og:image" content={company.logo} />
          <meta property="og:url" content={company.website} />
          <meta property="og:site_name" content={company.name} />
          <meta property="og:description" content={company.description || "Commandez vos produits chez " + company.name} />
          <meta name="twitter:title" content={company.name} />
          <meta name="twitter:image" content={company.logo} />
          <meta name="twitter:url" content={company.website} />

          <link rel="apple-touch-icon" sizes="180x180" href={company.logo} />
          <link rel="mask-icon" href={company.logo} color={company?.template?.theme?.color} />

          <meta name="msapplication-TileColor" content={company?.template?.theme?.color} />
          <meta name="theme-color" content={company?.template?.theme?.color} />
        </Head>
      )}
      <WithSubnavigation company={company} />
    </div>
  );
}

export default Header;
