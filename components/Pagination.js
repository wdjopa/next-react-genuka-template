import { ArrowForwardIcon, ArrowBackIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Select } from "@chakra-ui/react";
import React from "react";
import NextLink from "next/link";
import { useRouter } from "next/router";
function Pagination({ pagination }) {
  const router = useRouter();
  return (
    <Flex my="5" justifyContent="flex-end">
      <NextLink
        href={{
          pathname: router.pathname,
          query: { ...router.query, page: pagination.current_page - 1 },
        }}
        passHref
      >
        <Button mr="5" leftIcon={<ArrowBackIcon />} disabled={!pagination.prev} variant="outline">
          Previous
        </Button>
      </NextLink>
      <Select value={pagination.current_page} mr="5" width={"75px"} onChange={(e)=>{
        router.push({
          pathname: router.pathname,
          query: { ...router.query, page: e.target.value },
        });
      }} >
        {new Array(pagination.last_page).fill().map((_, index) => {
          let page_number = index + 1;
          return (
            <option key={Math?.random()} value={page_number}>
              {page_number}
            </option>
          );
        })}
        ;
      </Select>
      {/* {new Array(pagination.last_page).fill().map((_, index) => {
        let page_number = index + 1;
        if (page_number === pagination.current_page) {
          return (
            <NextLink
              href={{
                pathname: router.pathname,
                query: { ...router.query, page: page_number },
              }}
              passHref
              key={"key_" + index}
            >
              <Button mr="5" key={Math.random()}  variant="">
                {page_number}
              </Button>
            </NextLink>
          );
        }
        return (
          <NextLink
            href={{
              pathname: router.pathname,
              query: { ...router.query, page: page_number },
            }}
            passHref
            key={"key_" + index}
          >
            <Button mr="5"  variant="outline">
              {page_number}
            </Button>
          </NextLink>
        );
      })} */}
      <NextLink
        href={{
          pathname: router.pathname,
          query: { ...router.query, page: pagination.current_page + 1 },
        }}
        passHref
      >
        <Button rightIcon={<ArrowForwardIcon />} disabled={!pagination.next}  variant="outline">
          Next
        </Button>
      </NextLink>
    </Flex>
  );
}

export default Pagination;
