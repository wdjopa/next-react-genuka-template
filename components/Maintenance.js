import { EmailIcon, PhoneIcon } from "@chakra-ui/icons";
import { Box, Button, Center, Flex } from "@chakra-ui/react";
import React from "react";

function Maintenance({ company }) {
  return (
    <Box h="80vh" p="5vh">
      La boutique {company.name} est actuellement en maintenance. Pour passer vos commandes, contactez nous 
      <Flex>
        {company.email && (
          <Button as="a" leftIcon={<EmailIcon />} href={"mailto:" + company.email}>
            {company.email}
          </Button>
        )}
        {company.tel && (
          <Button as="a" leftIcon={<PhoneIcon />} href={"tel:" + company.tel}>
            {company.tel}
          </Button>
        )}
      </Flex>
    </Box>
  );
}

export default Maintenance;
