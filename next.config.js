module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["localhost", "dashboard.genuka.com", "bucket-my-store.s3.eu-west-3.amazonaws.com", "genuka.com"],
  },
};
