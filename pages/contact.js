import axios from "axios";
import Head from "next/head";
import React from "react";
import Layout from "../components/Layout";
import ProductList from "../components/products_collections/ProductList";
import { genuka_api_2021_10, TEMPLATE_NAME } from "../utils/configs";

function Collections({ company, products_list, pagination }) {
  return <Layout company={company}></Layout>;
}

// This gets called on every request
export async function getServerSideProps({ params, req, res, resolvedUrl, query }) {
  let protocol = "https:";
  let host = req ? req.headers.host : window.location.hostname;
  if (host.indexOf("localhost") > -1) {
    protocol = "http:";
  }
  let website_url = `${protocol}//${host}`;
  // Fetch data from external API
  let response = await fetch(`${genuka_api_2021_10}/companies/byurl?url=${website_url}`);
  const company = await response.json();

  if(company.tel)
  res.setHeader("location", "https://wa.me/" + company.tel.replaceAll("+", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("(", "").replaceAll(")", ""));
  else
  res.setHeader("location", "mailto:"+company.email)
  res.statusCode = 200;
  res.end();
}

export default Collections;
