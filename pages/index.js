import { Center, Spinner } from "@chakra-ui/react";
import Layout from "../components/Layout";
import Maintenance from "../components/Maintenance";
import FeaturedCollection from "../components/products_collections/FeaturedCollection";
import FeaturedProduct from "../components/products_collections/FeaturedProduct";
import Hero from "../components/sections/Hero";
import Testimonials from "../components/sections/Testimonials";
import { genuka_api_2021_10, TEMPLATE_NAME } from "../utils/configs";

// This gets called on every request
export async function getServerSideProps({ params, req, res, resolvedUrl, query }) {
  let protocol = "https:";
  let host = req ? req.headers.host : window.location.hostname;
  if (host.indexOf("localhost") > -1) {
    protocol = "http:";
  }
  let website_url = `${protocol}//${host}`;

  // Fetch data from external API
  let response = await fetch(`${genuka_api_2021_10}/companies/byurl?url=${website_url}`);
  const company = await response.json();

  return { props: { company } };
}

export default function Home({ company }) {
  if (!company)
    return (
      <Center>
        <Spinner />
        Chargement de la boutique
      </Center>
    );
  return (
    <Layout company={company}>
      {company.template &&
        company.template.sections &&
        company.template.sections.map((section, id) => {
          console.log({selected : section.selected_section, section})
          switch (section.selected_section) {
            case "image_and_text":
              return <Hero key={"section_"+id} section={section} color={company.template.theme.color} />;
            case "featured_collection":
              return <FeaturedCollection key={"section_" + id} company={company} collection_id={section.featured_collection} />;
            case "featured_product":
              return <FeaturedProduct key={"section_" + id}  company={company} product_id={section.featured_product} />;
            default:
              <></>;
          }
        })}
      {(!company.template || !company.template.sections) && <Maintenance company={company} />}
      {/* <Testimonials /> */}
    </Layout>
  );
}
