import axios from "axios";
import { useRouter } from "next/router";
import React from "react";
import Layout from "../../components/Layout";
import CollectionDetailPage from "../../components/products_collections/CollectionDetailPage";
import { genuka_api_2021_10, TEMPLATE_NAME } from "../../utils/configs";
import Head from "next/head";

function CollectionPage({ collection, company, products, pagination }) {
  const router = useRouter();
  return (
    <Layout
      company={company}
      meta_tags={
        <Head key="main">
          <title>
            Collection {collection.name} | {company.name}
          </title>
          <meta name="description" content={"Collection " + collection?.name + " - " + collection.description} />
          <meta name="keywords" content={collection?.description?.split(" ").join(", ")} />
          <meta name="author" content={"Genuka Platform"} />
          <link rel="icon" type="image/png" href={company.logo} />
          <meta name="robots" content="index, follow" />

          <meta property="og:title" content={`Collection ${collection.name} | ${company.name}`} />
          <meta property="og:image" content={(collection?.medias?.length > 0 ? collection.medias[0]?.link : company.logo) || company.logo} />
          <meta property="og:site_name" content={`Collection ${collection.name} | ${company.name}`} />
          <meta property="og:description" content={company.description || "Commandez vos produits chez " + company.name} />
          <meta property="og:url" content={router.basePath} />
          <meta name="twitter:url" content={router.basePath} />
          <meta name="twitter:title" content={`Collection ${collection.name} | ${company.name}`} />
          <meta name="twitter:image" content={(collection?.medias?.length > 0 ? collection.medias[0]?.link : company.logo) || company.logo} />

          <link rel="apple-touch-icon" sizes="180x180" href={company.logo} />
          <link rel="mask-icon" href={company.logo} color={company?.template?.theme?.color} />

          <meta name="msapplication-TileColor" content={company?.template?.theme?.color} />
          <meta name="theme-color" content={company?.template?.theme?.color} />
        </Head>
      }
    >
      <CollectionDetailPage company={company} collection={collection} products={products} pagination={pagination} />
    </Layout>
  );
}

// This gets called on every request
export async function getServerSideProps({ params, req, res, resolvedUrl, query }) {
  let protocol = "https:";
  let host = req ? req.headers.host : window.location.hostname;
  if (host.indexOf("localhost") > -1) {
    protocol = "http:";
  }
  let website_url = `${protocol}//${host}`;

  // Fetch data from external API
  let response = await fetch(`${genuka_api_2021_10}/companies/byurl?url=${website_url}`);
  const company = await response.json();

  const { per_page = 12, page = 1, sort_dir = "desc", sort_by = "created_at" } = query;
  const { id: collection_id } = params;
  console.log({collection_id, query})
  let url = `${genuka_api_2021_10}/companies/${company.id}/collections/${collection_id}?per_page=${per_page}&page=${page}&sort_by=${sort_by}&sort_dir=${sort_dir}`;
  response = (await axios.get(url)).data;

  return { props: { company, collection: response.collection, products: response.products.data, pagination: { ...query, ...response.products.links, ...response.products.meta } } };
}

export default CollectionPage;
