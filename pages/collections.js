import axios from "axios";
import Head from "next/head";
import React from "react";
import Layout from "../components/Layout";
import CollectionList from "../components/products_collections/CollectionList";
import { genuka_api_2021_10, TEMPLATE_NAME } from "../utils/configs";

function Collections({ company, collections_list, pagination }) {
  return (
    <Layout
      company={company}
      meta_tags={
        <Head key="main">
          <title>
             {company.name} - Collections
          </title>
          <meta name="description" content={"Catégories de l'entreprise " + company?.name} />
          <meta name="keywords" content={collections_list?.map((c) => c.name)?.join(", ") || "" + ", " + company?.name?.split(" ").join(", ")} />
          <meta name="author" content={"Genuka Platform"} />
          <link rel="icon" type="image/png" href={company.logo} />
          <meta name="robots" content="index, follow" />

          <meta property="og:title" content={"Catégories de l'entreprise " + company?.name} />
          <meta property="og:image" content={company.logo} />
          <meta property="og:url" content={company.website + "/collections"} />
          <meta property="og:site_name" content={"Catégories de l'entreprise " + company?.name} />
          <meta property="og:description" content={company.description || "Commandez vos produits chez " + company.name} />
          <meta name="twitter:title" content={"Catégories de l'entreprise " + company?.name} />
          <meta name="twitter:image" content={company.logo} />
          <meta name="twitter:url" content={company.website + "/collections"} />

          <link rel="apple-touch-icon" sizes="180x180" href={company.logo} />
          <link rel="mask-icon" href={company.logo} color={company?.template?.theme?.color} />

          <meta name="msapplication-TileColor" content={company?.template?.theme?.color} />
          <meta name="theme-color" content={company?.template?.theme?.color} />
        </Head>
      }
    >
      <CollectionList collections_list={collections_list} pagination={pagination} company={company} />
    </Layout>
  );
}

// This gets called on every request
export async function getServerSideProps({params, req, res, resolvedUrl, query }) {
  let protocol = "https:";
  let host = req ? req.headers.host : window.location.hostname;
  if (host.indexOf("localhost") > -1) {
    protocol = "http:";
  }
  let website_url = `${protocol}//${host}`;
  // Fetch data from external API
  let response = await fetch(`${genuka_api_2021_10}/companies/byurl?url=${website_url}`);
  const company = await response.json();

  const { per_page = 12, page = 1, sort_dir = "desc", sort_by = "created_at" } = query;
  let url = `${genuka_api_2021_10}/companies/${company.id}/collections?per_page=${per_page}&page=${page}&sort_by=${sort_by}&sort_dir=${sort_dir}`;
  response = (await axios.get(url)).data;
  const collections_list = response.data;

  console.log({ query, q: req.query, params, company, collections_list });
  // Pass data to the page via props
  return { props: { company, collections_list, pagination : {...query, ...response.links, ...response.meta} } };
}

export default Collections;
