import axios from "axios";
import Head from "next/head";
import React from "react";
import Layout from "../../components/Layout";
import { genuka_api_2021_10, TEMPLATE_NAME } from "../../utils/configs";
import { useRouter } from "next/router";
import SimpleProduct from "../../components/products_collections/SimpleProduct";


function ProductDetail({ company, product }) {
  const router = useRouter();
  return (
    <Layout
      company={company}
      meta_tags={
        <Head key="product">
          <title>
            {product.name} | {company.name}
          </title>
          <meta name="description" content={"" + product?.name + " - " + product.description} />
          <meta name="keywords" content={product?.description?.split(" ").join(", ")} />
          <meta name="author" content={"Genuka Platform"} />
          <link rel="icon" type="image/png" href={company.logo} />
          <meta name="robots" content="index, follow" />

          <meta property="og:title" content={`${product.name} | ${company.name}`} />
          <meta property="og:image" content={product?.medias[0]?.thumb || company.logo} />
          <meta property="og:url" content={company.website} />
          <meta property="og:description" content={company.description || "Commandez vos produits chez " + company.name} />
          <meta property="og:url" content={router.basePath} />
          <meta name="twitter:url" content={router.basePath} />
          <meta name="twitter:title" content={`${product.name} | ${company.name}`} />
          <meta name="twitter:image" content={product?.medias[0]?.thumb || company.logo} />

          <link rel="apple-touch-icon" sizes="180x180" href={company.logo} />
          <link rel="mask-icon" href={company.logo} color={company?.template?.theme?.color} />

          <meta name="msapplication-TileColor" content={company?.template?.theme?.color} />
          <meta name="theme-color" content={company?.template?.theme?.color} />
        </Head>
      }
    >
      <SimpleProduct company={company} product={product} />
      {/* <SimilarProducts />  */}
    </Layout>
  );
}

// This gets called on every request
export async function getServerSideProps({ params, req, res, resolvedUrl, query }) {
  let protocol = "https:";
  let host = req ? req.headers.host : window.location.hostname;
  if (host.indexOf("localhost") > -1) {
    protocol = "http:";
  }
  let website_url = `${protocol}//${host}`;

  // Fetch data from external API
  let response = await fetch(`${genuka_api_2021_10}/companies/byurl?url=${website_url}`);
  const company = await response.json();

  const { slug } = params;
  let url = `${genuka_api_2021_10}/companies/${company.id}/products/slug/${slug}`;
  response = await axios.get(url);
  console.log({slug, response});
  return { props: { company, product: response.data } };
}

export default ProductDetail;
