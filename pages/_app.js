import "../styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import { GenukaProvider } from "../store/genukaStore";

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <GenukaProvider>
        <Component {...pageProps} />
      </GenukaProvider>
    </ChakraProvider>
  );
}

export default MyApp;
